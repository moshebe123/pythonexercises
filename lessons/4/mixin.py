#-------------------------------------------------------------------------------
# Name:        OO MIXINs
# Purpose:
#
# Author:      Shimon
#
# Created:     10/11/2013
# Copyright:   (c) Shimon 2013
#-------------------------------------------------------------------------------

#
#
#  --- using MIXIN ==>> DECORATOR Design Pattern
#
#
class SCmenu :
    def __init__(self, mKot, mItems):
        self.kot = mKot
        self.lines = mItems

    def show (self):
        
        k = 0
        while k == 0 : # k == 0 => no proper choice
            print()
            self.showKot()
            self.showLines()
            k = self.getChoice()
        return k

    def showKot (self):   # use decorator ro change
        print(self.kot)

    def showLines(self):  # use decorator ro change
        for k,e in enumerate(self.lines):
            print ( str(k) + ': ' + e )

    def getChoice(self):  # use decorator to change
        s = ""
        s = input("Enter Choice: ")
        k = 0
        if len(s) > 0: k = int(s)
        if k < 1 or k > len(self.lines):
            print('!!!   Illegal input == ' + s)
            return 0
        return k


class SCmenu_kot1_mixin ():
    ''' design as MIXIN to class aaa '''
    def showKot (self):
        print(self.kot)
        print("-" *  len(self.kot))

class SCmenu_kotD_mixin():
    def showKot(self):
        print("$$$" + self.kot + "$$$")

class Menu_KotK_Mixin():
    def showKot(self):
        print("-" *  len(self.kot))
        print(self.kot)
        print("-" *  len(self.kot))

class Menu_LineQ_Mixin():
    def showLines(self):
        for k,e in enumerate(self.lines):
            print ( '[' + str(k) + ']: ' + e )

class SCmenu2dol(SCmenu_kotD_mixin, SCmenu):
    pass
class SCmenuKotK(Menu_KotK_Mixin, SCmenu):
    pass
class SCmenuLineS(Menu_LineQ_Mixin, SCmenu):
    pass


ls_menu = ['Open', 'New', 'Print']
def test_menu():
    m = SCmenu2dol('K O T E R E T', ls_menu)
    return m.show()

def test_menuk():
    m = SCmenuKotK('K O T E R E T', ls_menu)
    return m.show()

def test_menuk2():
    m = SCmenuLineS('K O T E R E T', ls_menu)
    return m.show()
