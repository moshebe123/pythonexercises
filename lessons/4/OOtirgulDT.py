import math

class StamXY:
    def __init__(self, x,  y):
        print("Enter StamXY __init__")
        self.x = x
        self.y = y
        self._z = 0
        self._z_counter = 0
        print("Exit StamXY __init__")

    def __bool__(self):
        return bool(self.x and self.y)

    def __call__(self, a):
        return a + self.x

    def __len__(self):
        a  = math.sqrt(self.x*self.x + self.y*self.y)
        return int(a)

    def __contains__(self, x):
        a = self.__len__()
        return x <= a

    def __ccopy__(self):
        # MUST USE: import copy
        return StamXY(self.x, self.y)

    def z_get(self):
        print("OUT ==> z_Get")
        return self._z

    def z_set(self, v):
        print("IN ==> z_Set")
        self._z_counter += 1
        self._z += v

    def get_z_counter(self) :
        return self._z_counter


    z = property(z_get, z_set, None, "z Doc")



class RStamXY(StamXY):
    def __init__(self, x, y):
        print("\nEnter RStamXY __init__")
        super().__init__(x,y)
        a = super()
        print(self.__class__.__mro__)
        print(self)
        print(a)
        print("Exit RStamXY __init__")
        print(super().__contains__(8))


obj0 = StamXY(0,0)
obj1 = StamXY(1,2)
obj2 = StamXY(3,4)
obj9 = RStamXY(5,6)


qqq= 1
zzz = 2

def funaaa():
    zzz= 3
    def funbbb():
        # global qqq
        # nonlocal zzz
        qqq= 4
        zzz= 5
        print('in funbbb :',zzz, qqq)
    funbbb()
    print('in funaaa:',zzz, qqq)

def test_fun_aaa_bbb():
    print()
    funaaa ()
    print('globals:',qqq, zzz)
