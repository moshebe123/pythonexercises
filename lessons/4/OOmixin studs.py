#-------------------------------------------------------------------------------
# Name:        OO MIXINs
# Purpose:
#
# Author:      Shimon
#
# Created:     10/11/2013
# Copyright:   (c) Shimon 2013
#-------------------------------------------------------------------------------

#
#
#  --- using MIXIN ==>> DECORATOR Design Pattern
#
#
class SCmenu :
    def __init__(self, mKot, mItems):
        self.kot = mKot
        self.lines = mItems

    def show (self):
        
        k = 0
        while k == 0 : # k == 0 => no proper choice
            print()
            self.showKot()
            self.showLines()
            k = self.getChoice()
        return k

    def showKot (self):   # use decorator ro change
        print(self.kot)

    def showLines(self):  # use decorator ro change
        for k,e in enumerate(self.lines):
            print ( str(k + ': ' + e) )

    def getChoice(self):  # use decorator to change
        s = ""
        s = input("Enter Choice: ")
        k = 0
        if len(s) > 0: k = int(s)
        if k < 1 or k > len(self.lines):
            print('!!!   Illegal input == ' + s)
            return 0
        return k


class SCmenu_kot1_mixin ():
    ''' design as MIXIN to class aaa '''
    def showKot (self):
        print(self.kot)
        print("-" *  len(self.kot))

