#-------------------------------------------------------------------------------
# Name:         Start
# Purpose:      Start examples
# Author:       Dr Shimon Cohen
# Created:      June 2016
# Copyright:    (c) Shimon 2016
#
# Topics: simple function, variables, print
#       multi-values, math (module), import
#       Comment, DOCSTRING, None, string
#       PEP = Python Enhancement Proposal
#-------------------------------------------------------------------------------
import sys
def test_assert(x):
    z = 3
    try:
        if x==1: test_assert()
        assert x> 5, "X is " + str(x)
        print ('ok')
    except AssertionError as e:
        print(e.args)
    except Exception as e:
        print("E ", e.args)
    except :
        print("Unexpected ", sys.exc_info()[0])
