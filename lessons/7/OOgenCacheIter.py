#-------------------------------------------------------------------------------
# Author:      Shimon
#
# Created:     September 2016
# Copyright:   (c) Shimon 2016
#-------------------------------------------------------------------------------

class FiboGen:

    def __init__(self, n):
        self.n = n
        self.k = 0
        self.a = 0
        self.b = 1

    def __iter__(self):
        return self

    def __next__(self):
        ''' yield next fibo up to a limit '''
        if self.k > self.n:
            raise StopIteration
        self.k = self.k + 1
        self.a , self.b = self.b, self.a + self.b
        return self.b

def test_FiboGen(n):
    a = FiboGen(n)
    for k in a:
        print(k)
# -----------------------------


class FiboGenCache:
    def __init__(self, limit=None):
        print('FiboGenCache')
        self.n = 2
        self.ls = [0,1,1]
        self.limit = limit

    def __iter__(self):
        print('FiboGenCache __iter__')
        r = FiboIter(self)
        return r

    def __len__(self):
        return self.n

    def __getitem__(self, k):
        if k <= self.n :
            return self.ls[k]
        print('__getitem__ ',self.n, k, self.limit)
        if self.limit:
            if k > self.limit :
                print('FiboGenCache IndexError')
                raise IndexError
        for j in range(self.n+1, k+1):
            z = self.ls[j-1] + self.ls[j-2]
            #print(z, self.ls)
            self.ls.append(z)
        self.n = k
        return self.ls[k]

fiboObj = FiboGenCache (10)

def test_Fibo_Item(n):

    for z in zip(range(n+1),fiboObj) :
        print(z)

# -----------------------------

class FiboIter:
    def __init__(self, obj):
        print('FiboIter')
        self.myobj = obj
        self.k = 0

    def iter(self):
        print('FiboIter __iter__')
        self.k = 0
        return self

    def __next__(self):
        limit =  self.myobj.limit
        if limit:
            if self.k > limit:
                print('FiboIter StopIteration')
                raise StopIteration

        result = self.myobj[self.k]
        self.k += 1
        return result


def test_get_cache_iter1(n):
    for k,fibo in zip( range(n+1), fiboObj):
        print(k, fibo)

def test_get_cache_iter2():
    for fibo in fiboObj:
        print(fibo)

