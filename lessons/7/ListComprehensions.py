def tt1 () :
    sq = ( (x, y, x*y) for x in range(11,20)
                       for y in range(11,20)
                       if (x%2==1) and (y%2==1))
    print(list(sq))



def tt2 () :
    sq = ( (x, y, x*y) for x,y in zip( range( 11, 20 ), range( 11, 20 ))
                      if ( x % 2 == 1 ) and ( y % 2 == 1 ))
    print(list(sq))

def test_tt12 (what):
	if what == 1: tt1()
	if what == 2; tt2()