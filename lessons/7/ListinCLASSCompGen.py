import random

def random_generator( n, upto = 100):
    ''' random_gen( n, upto = 100)
    generates <n> random with random values from 1 upto and includes 100
    '''
    while(n):
        n -= 1
        yield random.randint(1, upto)

def test_random_gen(n):
    print('\n RANDOM ', n )
    g = random_generator(n)
    for x in g: print(x)

def factors_generator(n):
    k = 2
    while n > 1 :
        if n % k == 0:
            yield (k,n)
            n = n // k
        else:
            k += 1

def test_factors_generator(n):
    print('\n Factors ', n)
    g = factors_generator(n)
    for x in g: print(x)

def f321_generator(n):
    if n == 1 :
        yield 1
        return
    yield n
    z = n // 2 if (n%2==0) else n*3+1
    for w in  f321_generator(z):
        yield w

def test_f321_generator(n):
    print('\n f 321', n)
    g = f321_generator(n)
    for x in g: print(x)

def deep_list_generator(ls):
    for z in ls:
        if not isinstance(z, list):
            yield z
        else:
            for w in deep_list_generator(z):
                yield w
ls = [1,2,3, [11, [77, 88], 22, 33, 44],4,5,6,7,8,9]
def test_deep_list_generator(ls):
    print('\n deep_list_generator', ls)
    g = deep_list_generator(ls)
    for x in g: print(x)
