#-------------------------------------------------------------------------------
# File Name:    OOworkStud - Shimon Way
# Purpose:      OO Worker Stud Muliti inheritance
#
# Author:       Dr. Shimon Cohen
#
# Created:      7/8/2016
# Copyright:    (c) Shimon 2016
# Licence:     
#-------------------------------------------------------------------------------

class Person:
    """ base class for different kinds of Person Student Worker """
    def __init__(self, name, phone):
        ''' __init__(self, name, phone)
            avoid repeat __init__
        '''
        print('Person __INIT__  start', name)
        
        if hasattr(self, 'name'): # avoid double calls to: __init__ 
            print('Person __INIT__  ALREADY !!!', name)
            return
        
        self.name = name
        self.phone = phone
        print('Person __INIT__  end', name)

    def __str__(self):
        ''' __str__(self)   used in: print(obj)        '''
        
        return 'Person: ' + self.name
    
    def __repr__(self):
        ''' __repr__(self)  used when typeing: obj    '''
        sFormat = 'Person( {0!r}, {1:d} )'
        s = sFormat.format(self.name, self.phone)
        return s
	


class Worker(Person):
    ''' describe a Worker     '''
    def __init__(self, name, phone, darga, vetek):
        print('Worker __INIT__  start', name)
        Person.__init__(self, name, phone)
        self.darga = darga
        self.vetek = vetek
        print('Worker __INIT__  end', name)

    def __repr__(self):
        ''' __repr__(self)  used when typeing: obj     '''
        sFormat = 'Worker( {0!r}, {1:d}, {2:d}, {3:d} )'
        s = sFormat.format(self.name, self.phone, self.darga, self.vetek)
        return s

    
    def compute_salary(self):
        ''' compute_salary(self)         '''
        a = self.darga * 3000
        b = a * (100 + self.vetek) / 100
        print('Worker salary= ', b)
        return b


class Student(Person):
    ''' describe a Worker
    '''
    def __init__(self, name, phone, toar,  **kw ):
        print('Student __INIT__  start', name)
        Person.__init__(self, name, phone)
        self.toar = toar
        print('Student __INIT__  end', name)

    def __repr__(self):
        ''' __repr__(self)  used when typeing: obj     '''
        sFormat = 'Student( {0!r}, {1:d}, {2!r} )'
        s = sFormat.format(self.name, self.phone, self.toar)
        return s
        

    def compute_pay_limud(self):
        ''' compute_pay_limud(self)
        '''
        # s = 0 # default
        #if self.toar == "BA": s = 10000
        #if self.toar == "MA": s = 15000
        #if self.toar == "Phd": s = 12000

        dict_pays_limud = {"BA": 10000, "MA":15000, "Phd": 12000}
        #s = dict_pays_limud.get[self.toar]
        s = dict_pays_limud.get(self.toar, 0)
        print("Student limud= ", s)
        return s
    

class SW_Student_Worker( Worker, Student):
    ''' SW_Student_Worker( Worker, Student)
        multipl inheritance
    '''
    def __init__(self, name, phone, darga, vetek, toar ):
        print('SW __INIT__  start', name)
        Worker.__init__(self, name, phone, darga, vetek)
        Student.__init__(self, name, phone, toar)
        print('SW __INIT__  end', name)

    def __repr__(self):
        ''' __repr__(self)  used when typeing: obj     '''
        sFormat = 'Worker( {0!r}, {1:d}, {2:d}, {3:d}, {4!r} )'
        s = sFormat.format(self.name, self.phone, self.darga, self.vetek, self.toar)
        return s

    def compute_salary(self):
        ''' compute_salary(self)
        '''
        work_salary = Worker.compute_salary(self)
        stud_payment = self.compute_pay_limud() / 12
        
        sFormat = 'Salary= {0} limud={1:6.0f} monthly'
        print( sFormat.format(work_salary, stud_payment)) 

        return work_salary - stud_payment
        

    def compute_pay_limud(self):
        ''' compute_pay_limud(self)
        '''
        stud_payment = Student.compute_pay_limud(self)

        s = stud_payment * (1 - self.darga * 0.1) # default
        
        print ('Worker limud= ' , s)
        return s


def main():
    global p1, w1, s1, sw1 , sw2
    p1 = Person(name='Shim Ko', phone=3434)
    w1 = Worker(name='Shimon Cohen', phone=87878, darga=2, vetek=10)
    s1 = Student(name='Jacob Levy', phone=12121, toar="BA")
    sw1 = SW_Student_Worker(name='Moshe Vardi', phone=878, darga=3, vetek=8, toar='BA')
    sw2 = SW_Student_Worker(name='Yosi Mor', phone=111, darga=5, vetek=5, toar='Phd')

    print('\n', sw1, sep='')
    print(sw1.compute_salary ())
    print('\n', sw2, sep='')
    print(sw2.compute_salary ())
    
if __name__ == '__main__':
    main() # write tests in main (see above)
