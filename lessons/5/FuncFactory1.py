# from  scHelp import *

def isEven(x): return (x % 2) == 0
def select_make(f):
    return (lambda L: filter(f,L))

select_evenm = select_make(isEven)

def test_select_make():
	print(list(select_evenm(range(19))))

	
test_select_make()


LLL = [12,[3,[5],[6,[[7, [],8], []]]], [8, [0,-787,23,434,444,33]]]

def factory_dolist_deep_A( fa ):
    L = []
    def ListFunk(L, f = fa):

        for x in L:
            if(isinstance(x,list)):
                ListFunk(x)
            if(isinstance(x,int)):
                x = f(x)
                print(x)
    return ListFunk

#returning from factory
list_add = factory_dolist_deep_A((lambda x: x + 1))
list_mult2 = factory_dolist_deep_A ( (lambda x: x * x) )
#cheking funks
list_add(LLL)
print('----------------------')
list_mult2(LLL)


# def testfile1():
#     add1 = factory_plus(1)
#     print ('add1(3): ', add1(3))

#     add2 = factory_plus(2)
#     print ('add2(7): ', add2(7))

#     sub1 = factory_plus(-1)
#     print ('sub1(8 ): ', sub1(8))
#     print ('sub1(0 ): ', sub1(0))
#     print ('sub1(-4): ', sub1(-4))

#     ribuah = factory_pow(2)
#     print ('ribuah(5): ', ribuah(5))
#     print ('ribuah(2): ', ribuah(2))

#     zbShlishit = factory_pow(3)
#     print ('zbShlishit(7): ', zbShlishit(7))
#     print ('zbShlishit(2): ', zbShlishit(2))

#     Shoresh = factory_pow(0.5)
#     print ('Shoresh(16): ', Shoresh(16))
#     print ('Shoresh(2):  ', Shoresh(2))

#     zShoresh5 = factory_pow(0.2)
#     print ('zShoresh5(32): ', zShoresh5(32))
#     print ('zShoresh5(1024): ', zShoresh5(1024))


# if __name__ == '__main__':
#     pass

# # -------------- Factory and Closure A =========

# import math

# def factory_plus(n):
#     ''' make a function that adds n to x '''
#     def addn (x):
#         return x + n
#     return addn

# def factory_pow(n):
#     ''' make a function that computes to the power of n '''
#     def pown (a):
#         return math.pow(a,n)
#     return pown

# def zzribuah(a):
#     return math.pow(a, 2)



# def testFile2 ():
#     LN = [1,2,3,4,5]
#     print('\n\n')
#     print ('apply f to list: ', LN)

#     list_pow2 = factory_f_on_list ( (lambda x: x * x) )
#     ''' pow2 skdjskdjksjdk '''
#     print ('list_pow2(LN)) : ', list_pow2(LN))

#     list_mult2 = factory_f_on_list ( (lambda x: x * 2) )
#     print ('list_mult2(LN)): ', list_mult2(LN))

#     list_sum = factory_gen_on_list(( lambda a: a), ( lambda va, r: va + r), 0)
#     print ('list_sum(LN)): ', list_sum(LN))

#     list_sum2 = factory_gen_on_list(( lambda a: a*a), ( lambda va, r: va + r), 0)
#     print ('list_sum2(LN)): ', list_sum2(LN))

#     list_mult = factory_gen_on_list(( lambda a: a), ( lambda va, r: va * r), 1)
#     print ('list_mult(LN)): ', list_mult(LN))

#     list_sum_1divn = factory_gen_on_list(( lambda a: 1/a), ( lambda va, r: va + r), 1)
#     print ('list_sum_1divn(LN)): ', list_sum_1divn(LN, 1))

#     def multLcomb(r, va ):
#         vlast = 1
#         if r : vlast = r[-1]
#         r.append(va * vlast)
#         return r
#     list_multL = factory_gen_on_list(( lambda a: a), multLcomb, [])
#     print ('list_multL(LN)): ', list_multL(LN))



# def factory_f_on_list(f):
#     ''' define a function that loops on a list
#     and apply the function f to each member of a list '''
#     def app_f_on_list (L):
#         Lnew = []
#         for a in L:
#             Lnew.append(f(a))
#         return Lnew

#     return app_f_on_list

# def factory_gen_on_list( fa, fcomb, v1 = 0):
#     ''' v1 => start value for result
#     fa => an unary function to apply to each element in the list L
#     fcomb => binary function that combines  fa(a) with result
#     '''
#     def gen_on_list(L , printFlag = 0):
#         if not L: return v1
#         r = v1
#         for a in L:
#             va = fa(a)
#             r = fcomb(r,va)
#             if printFlag == 1: print(va)
#         return r
#     return gen_on_list

# ls = [1, 2, 3, 4, 5]
# fn_sum = factory_gen_on_list((lambda x:x), (lambda x, y: x + y), 0)
# fn_mul = factory_gen_on_list((lambda x:x), (lambda x, y: x * y), 1)
# fn_str = factory_gen_on_list((lambda x:x), (lambda x, y: str(x) + ", " + str(y) if x else y), '')


# list_pow2 = factory_f_on_list ( (lambda x: x * x) )
# list_pow2.__doc__ = " list pow2 skdjskdjksjdk"
# # ----------- show closure ---------

# aaa = 1

# def factory_f_with_free_vars():
#     def fffa(x):
#         global aaa
#         # nonlocal aaa
#         aaa = aaa + x
#         print('aaa b', aaa)
#     return fffa

# def testFile3 ():
#     z = factory_f_with_free_vars()
#     print('aaa a', aaa)
#     z(8)
#     print('aaa c', aaa)



# if __name__ == '__main__':
#     testfile1()
#     testFile2()
#     testFile3()
# print('global aaa: ', aaa)