from  scHelp import *

qqq     = 1
zzz     = 2

def funaaa():
    zzz= 3
    def funbbb():
        # global qqq
        # nonlocal zzz
        qqq= 4
        zzz= 5
        print('in funbbb :',zzz, qqq)
    funbbb()
    print('in funaaa:',zzz, qqq)

funaaa ()
print('globals:',qqq, zzz)












def f (x):
    """
    __dict__ and locals() are different !!!
    locals starts empty and defines local variables and fns
    """
    def def_function_g():
        if not hasattr (f,'g'):
            def g (b): # default function g
                return b + 1
        else:
            g = getattr (f, 'g')
        
    # global locals # X
    print(f.__dict__)
    print(locals())

    def_function_g()
    print(locals())
    print(g(x))

    g = (lambda z: z + 2)
    print(locals())
    print(g(x))

    #print(locals())
    #locals = (lambda : 88) # X change def of locals()
    #print(locals()) # X
    zz = getattr (f,'z')
    print(zz(9))

gg = (lambda z: z + 2)

d = f.__dict__
print(d)

d['z'] = (lambda a: a * 2)
d['g'] = (lambda a: a + 99)

def mapFNs(LF, a):
    R = []
    for f in LF:
        R.append(f(a))
    return R

def test_mapFNs():
    f1 = (lambda x: x + 1)
    f2 = (lambda x: x + x)
    f3 = (lambda x: x * x)
    f4 = (lambda x: x ** 4)
    print(mapFNs([f1,f2,f3, f4], 4))

def rootK( a, k ):
    def closeEnough(s1,s2):
        return abs(s2-s1) < 0.0000001
    if a > 1:
        s1 , s2 = (1,a)
    else:
        s1, s2 = (a, 1)
    while not closeEnough(s1,s2):
        x = (s1+s2)/2
        print(a,k,s1,s2, x)
        if (x**k) > a:
            s2 = x
        else:
            s1 = x
    return x

def rootKR( a, k ):
    def closeEnough(s1,s2):
        ''' check interval is small enough '''
        return (s2-s1) < 0.000000000000001
    def avr2(a,b): return (a + b) / 2
    def doRoot(a, k, s1, s2):
        ''' recursive solution '''
        print(s1,s2)
        newx = avr2(s1,s2)
        if closeEnough(s1,s2):
            return    newx

        if (newx ** k) > a:
            doRoot(a, k, s1, newx)
        else:
            doRoot(a, k, newx, s2)

    doRoot(a,k,1,a)

def rootKRL( a, k=2, epsi=0.0000001 ):

    def doRoot(s1, s2):
        '''  recursive solution '''
        def avr2():
            #nonlocal s1,s2
            return (s1 + s2) / 2

        def closeEnough():
            ''' check interval is small enough '''
            #nonlocal epsi, s1, s2
            return (s2-s1) < epsi

        # ============ body of doRoot

        #nonlocal a,k
        newx = avr2()
        if closeEnough(): return newx

        if (newx ** k) > a: return doRoot(s1, newx)
        else:               return doRoot(newx, s2)

    def isit_INT(x):
        nonlocal epsi
        if abs( round(x) - x) < epsi : return round(x)
        return x

    #  ============ body of
    x = doRoot(1, a)
    return isit_INT(x)

def test_root_fns():
	print(rootKRL(81))
	print(rootKRL(84, 4))
	print(rootKRL(81, 4))

	
