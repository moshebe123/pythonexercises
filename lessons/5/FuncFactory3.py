#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Shimon
#
# Created:     27/10/2013
# Copyright:   (c) Shimon 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import math

# -------------- Factory and Closure C =========

ls = [11, 34, 3, 76, 45, 23, 89, 43, 2, 44, 78]

class SCbag :
    def __init__ (self ):
        pass

    def __call__(self):
        pass

    def finish(self):
        pass

class SCbag6(SCbag):
    def __init__(self, ls):
        self.max = - math.inf
        self.min = math.inf
        self.sum = 0
        self.count = 0
        self.n_evens = 0
        self.avr = 0

    def __str__(self):
        s = "SCbag5:"
        s = s + '\n max     = ' + str(self.max)
        s = s + '\n min     = ' + str(self.min)
        s = s + '\n sum     = ' + str(self.sum)
        s = s + '\n count   = ' + str(self.count)
        s = s + '\n n_evens = ' + str(self.n_evens)
        s = s + '\n avr     = ' + str(self.avr)
        return s


    def __call__ (self, a ):
        if a > self.max : self.max = a
        if a < self.min : self.min = a
        self.sum += a
        self.count += 1
        if a %2 == 0: self.n_evens += 1

    def finish(self):
        self.avr = self.sum / self.count


def factory_obj_bag( bag_obj_type ) :
    def do_list_bag(ls):
        bag = bag_obj_type(ls)
        for a in ls:
            bag(a)
            #print(d_bag)
        bag.finish()
        return bag

    return do_list_bag

def test_bag_obj (ls=ls):
    do_obj_bag = factory_obj_bag(SCbag6)
    do_obj_bag.__doc__ = 'returns bag with fields [max, min, sum, n_even, count, avr]'
    print('BAG OBJ')
    print(ls)
    bag = do_obj_bag(ls)
    print(bag)
    return bag


def factory_list_bag( fstart, fa, fresult ):
    def do_list_bag(ls):

        d_bag = fstart(ls)
        print(d_bag)
        for a in ls:
            d_bag = fa(a, d_bag)
            print(d_bag)
        return fresult(d_bag)
    return do_list_bag

def do_start(ls):
    return [ls[0], ls[0], 0, 0, 0 ]

def do_fa ( a , bag ):
    if bag[0] < a: bag[0] = a # max
    if bag[1] > a: bag[1] = a #min
    bag[2] += a  # sum
    if (a % 2) == 0: bag[3] +=1 # n even
    bag[4] += 1 # count
    return bag

def do_fin(bag):
    bag.append(bag[2]/bag[3])
    return bag

# bag => 0=maximum 1=count evens, 2=sum 3=count
def test_bag_list():
    do_list_bag = factory_list_bag(do_start, do_fa, do_fin)
    do_list_bag.__doc__ = 'returns [max, min, sum, n even, count, average]'
    print('BAG list')
    print(ls)
    bag = do_list_bag(ls)
    print(bag)
    return bag

