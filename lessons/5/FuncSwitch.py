
def fn_with_switch (k):
    """
    simulate a Switch statement
    :param k: sekected case
    :return:
    """
    def case1():         print("sw 1")
    def case2():         print("sw 2")
    def case3():         print("sw 3")
    def default () :     print("default")

    ls_cases = [None, case1, case2, case3 ]

    if k < 1 or k > 3 :
        default()
    else:
        # ls_cases[k]()
        zcase = ls_cases[k]
        zcase()

def fn_with_switch1 (k):
    """
    use zswitch to simulate switch
    :param k: choose case
    :return:
    """
    def case1():     print("sw 1")
    def case2():     print("sw 2")
    def case3():     print("sw 3")
    def default () : print("default")

    ls_cases = {1: case1, 2:case2, 3:case3, 'default' : default }
    sc_switch(k, ls_cases)

def sc_switch (k, zDictCases):
    """
    simulate switch statement with cases in Dict
    'default' is used if case was not found
    if 'default is not defined in
    :param zDictCases: dictionary with cases
    :param k: selected case
    :return:
    """
    if k in zDictCases:
        fn = zDictCases[k]
        return fn()

    if 'default' in zDictCases :
        fn = zDictCases['default']
        return fn()

    return None

def fn_with_switch2 (k):
    """
    test Zswitch
    :param k: selected case
    :return:
    """
    Z = 111
    def do2 (): # changes Z value
        nonlocal Z
        Z = 4
        return 444
    #
    cases = {
         1          : (lambda : Z + 11 ),
         7          : (lambda : Z + 22 ),
         3          : (lambda : Z + 33 ),
         2          : do2,
         'default'  : (lambda : Z  )
         }
    #
    print(sc_switch(k, cases))
    print(Z)

def fn_with_switch3 (k):
    """
    test Zswitch
    :param k: selected case
    :return:
    """
    Z = 111
    def do2 (): # changes Z value
        nonlocal Z
        Z = 4
        return 444

    result = sc_switch( k, {
         1          : (lambda : Z + 11 ),
         7          : (lambda : Z + 22 ),
         3          : (lambda : Z + 33 ),
         2          : do2,
         'default'  : (lambda : Z  )
         })
    #
    print( result )
    print( Z )


def zCond (* L_conds):
    """ implements COND """

    for c2 in L_conds:
        if c2[0] == True: return c2[1]()
        if c2[0]() :      return c2[1]()

def zCond_test1 (x, y):
    """
    Test zCond == >> print relations
    :param x, y: two numbers
    :return:
    """
    zCond(
        (lambda : x <  y,   lambda: print ("x <  y")),
        (lambda : x == y,   lambda: print ("x == y")),
        (True  ,            lambda: print ("x >  y"))
        )

def zCond_GCD (x, y):
    """
    Use zCond to compute GCD ( = Greater Common Divisor )
    :param x, y: two positive integers
    :return: GCD = integer
    """
    print (x,y)
    return (zCond(
        (lambda : x == 0,   lambda: y),
        (lambda : y == 0,   lambda: x),
        (lambda : y == x,   lambda: x),
        (lambda : y > x,    lambda: zCond_GCD(x, y % x)),
        (True  ,            lambda: zCond_GCD(x % y, y))
        ))
