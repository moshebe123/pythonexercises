#-------------------------------------------------------------------------------
# Name:         FuncLambda
# Purpose:      Lambda functions
# Author:       Dr Shimon Cohen
# Created:      June 2016
# Copyright:    (c) Shimon 2016
#
# 
#-------------------------------------------------------------------------------

def test_lambda_1 (a):
    """ a is a number """ 
    fn = (lambda x: x * x)
    return fn(a)

LL = [1,2,3,4,5,6,7,8,9,10]

def test_lambda_map_1 (L):
    """ L is a list of numbers """ 
    fn = (lambda x: x * x)
    return map(fn, L)

def test_lambda_map_2 (L):
    """ L is a list of numbers """ 
    return map((lambda x: x * x), L)

def test_lambda_filter_2 (L):
    """
    L is a list of numbers
    returns those bigger than 5
    """ 
    return filter((lambda x: x > 5), L)


# define a function using lambda
#  and if inside
lamb_if1 = (lambda x : x * 2 if x  else x+1)

def test_lamb_if1 (a):
    return lamb_if1(a)
