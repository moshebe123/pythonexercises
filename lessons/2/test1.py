L1 = [3, 5, 7, 8, 9]
L2 = ["abc", [3,2, 5], 5, 'abc', [8, 9, 4, "aaaaa"]]


def check_type_1(i):
	if isinstance(i, int):
		return 'Integer'
	elif isinstance(i, str):
		return 'String'
	elif isinstance(i, list):
		return 'List'
	elif isinstance(i, float):
		return 'Float'
	else:
		return "I don't know..."

def check_type_2(i):
	if type(i) == int:
		return 'Integer'
	elif type(i) == str:
		return 'String'
	elif type(i) == list:
		return 'List'
	elif type(i) == float:
		return 'Float'
	else:
		return "I don't know..."


# Test 4+5
# for i in L2:	
# 	print (check_type_1(i), '-' , check_type_2(i))

# Test 6-9
# tempL = L1.copy()
# tempL.append('1')
# print ('Testing append: ', tempL)

# tempL = L1.copy()
# tempL.extend(tempL)
# print ('Testing extend: ', tempL)

# L3 = L1.copy()
# print ('L1==L3 ?', L1==L3, ' L1 is L3 ?' , L1 is L3)

# L4 = L1
# print ('L1==L4 ?', L1==L4, ' L1 is L4 ?' , L1 is L4)

# Nice trick for printing matrix like pattern
# for a,b,c in [['A','B','C'],[1,2,3], [4,5,6], [7,8,9]]:
# 	print(a,b,c)

#Note: if we put 3 iteration params, Every nested list should be in size of 3 exactly!
# for a,b in [[1,1,1],['A','B','C'],[1,2,3], [4,5,6], [7,8,9]]:
# 	print (a,b)

# This will work - single argument is handled differently...
# for t3 in [['a','b','c'],[1,2,3], [4,5,6,10], [7,8]]:
# 	print(t3)


# def func(x):
# 	return x%2

# print (list(map(func,range(5))))
# print (list(filter(func,range(5))))

L = [1,2,3,4,5]

def move_start_2_end(L, n):
	return L[n:] + L[:n]

def print_gt_n(L, n):
	return (list(filter(lambda x: x > n, L)))

def print_gt_n(L, n):
	return (list(filter(lambda x: x > n, L)))

def make_list_to_pairs(L):
	return [list(i) for i in zip(L[0::2], L[1::2])]

# print (move_start_2_end(L, 2))
# print (print_gt_n(L, 2))
print (make_list_to_pairs(L))


