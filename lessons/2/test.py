#-------------------------------------------------------------------------------
# Name:         Func in Class
# Purpose:
# Author:       Dr Shimon Cohen
# Created:      June 2016
# Copyright:    (c) Shimon 2016
#
#-------------------------------------------------------------------------------

q = 22
def foo (x):
    q = 33
    def foo_A(y):
        return (q,x,y)
    return foo_A

        

def make_add(n):

    def add(x):
        return x + n
    
    return add



z = 1
def f1(w):
    x = w ; y = 3

    def f2(k):
        nonlocal x
        x = k 
        return x, y, w

    def f3(k):
        nonlocal y
        y = k 
        return x, y, w

    return f2, f3

ls = [2, 3, 5, 8]

def make_list_comp( fn_start, fn_comb, fn_end):
    ''' comment make_list_comp '''
    def lsc (ls):
        v = fn_start(ls)  # v = 1
        for x in ls:
            v = fn_comb(v,x) # v = v * x
        return fn_end(v,ls)  # v 
    
    return lsc


# f ( range(1,7) )
def fa_start (ls) :
    return 0
def fa_comb(v,x):
    return x + v
def fa_end(v,ls):
    return v / len(ls)


suml = make_list_comp( (lambda ls :  0),
                       (lambda v,x:  v + x),
                       (lambda v, ls: v ))

mul_ls = make_list_comp((lambda ls :  1),
                       (lambda v,x:  v * x),
                       (lambda v, ls: v ))



#
ls = [ 7, 5 ,6 ,9, 3, 4, 2, 4]


def isEven(x): return (x % 2) == 0

def select_make(f):

    def lmap (ls):
        return list(filter(f,ls)) 

    return lmap

select_even = select_make(isEven)

def test_select_make():
	print(list(select_even(range(19))))
# ------------------

def  fn_with_counter():
    k = 0

    def f (a = None):
        nonlocal k
        if a == None : return k
        k += 1
        return a*a

    return f










def add2(x,y):
    
    return x+y
def add3(x,y, z):
    print(x,y,z)
    return x+y+z



def wrf (*args, **kwargs):
        print(args, kwargs)
        
        func = add3
        
        s = str(args)
        if kwargs: s += ' ' + str(kwargs)
        print('Function ' + func.__name__ + ' : ' + s)

        _result = func(*args, **kwargs)

        print('Result of '  + func.__name__ + ' : ' + str(_result))
        
        return _result









	
def counter_wrapper(func):
    k = 0
    def wrf (*args, **kwargs):
        nonlocal k
        _result = func(*args, **kwargs)
        k += 1
        return _result

    wrf.getk = ( lambda : k) 

    return wrf

def negative_wrapper(func):
    
    def wrf (*args, **kwargs):
        
        _result = func(*args, **kwargs)
        if _result < 0 :
            print( func.__name__ + ' is negative ' + str (_result)) 
        return _result

    return wrf

#f = negative_wrapper (add2)
# add2 = f











# nwrpadd2 = negative_wrapper(add2)
# nwrpadd2(3, -7)








@counter_wrapper
@negative_wrapper
def mul2(x,y):
    return x*y




def info_wrapper(func):
    
    def wrf (*args, **kwargs):
        
        s = str(args)
        if kwargs: s += ' ' + str(kwargs)
        print('Function ' + func.__name__ + ' : ' + s)

        _result = func(*args, **kwargs)

        print('Result of '  + func.__name__ + ' : ' + str(_result))
        return _result

    return wrf




import functools
@functools.lru_cache(maxsize = None)
def  fibw(n):
    if n <= 2 : return 1
    return fibw(n-1) + fibw(n-2)








def  add1 (x):
	return x+1
def  sub1 (x):
	return x-1
def  plus (x,y):
	return x+y

def  do(f, *a):
        print(a)
        return f(*a)

def getFN(k):
        if k == 1: return add1
        if k == 2: return sub1
        if k == 3: return plus

def   map_list (f, ls_in): # similar to  map
    ls = []
    for k in ls_in:
        resultfk = f(k)
        ls.append(resultfk)
        print(k, resultfk, ls)
    return ls

def count_ls_deep(ls_in):
    count = 0

    def do(ls):
        nonlocal count
        if isinstance(ls,list):
            for x in ls: k = do(x)
        else: count += 1

    do(ls_in)
    return count

def rev(ls):
    def rev2(lsin, lsout):
        print(lsin, ' < === >', lsout)
        if lsin:
            return rev2(lsin[1:],[lsin[0]] + lsout)
        return lsout
    
    rev2(ls, [] )

qqq     = 1
zzz     = 2

def funaaa():
    #global zzz
    zzz = 3
    def funbbb():
        global qqq, zzz
        #nonlocal zzz
        qqq= 4
        zzz= 5
        print('in funbbb :',zzz, qqq)
    funbbb()
    print('in funaaa:',zzz, qqq)

#funaaa ()
#print('globals:', zzz, qqq)
#nonlocal a,b

a = 5
b = 6
def g1 (a, b):
        print("f1 A", a,b)
        a = 23
        def g2(c , d):
                nonlocal a
                a = 7  + b 
                print("f2 B", a,b,c,d)

        print("f1 B", a,b)
        g2(3,4)
        print("f1 C", a,b)
#g1(1,2)
#print("globals ", a,b)



def rootk_rec(a , k, epsi = 0.00001):

    def rootk_rec_X(s1, s2):

        x = (s1 + s2) /2
        #print (s1, x, s2)
        print ("%3.5f < %3.5f > %3.5f" % (s1,x,s2))
        if s2 - s1 <  epsi : return x

        if x ** k > a :
            return rootk_rec_X(s1,x)
        else:
            return rootk_rec_X(x,s2)
        # body of rootk_rec 
    return rootk_rec_X(1,a)


def rootk(a, k, epsi = 0.00001):
    s1 = 1
    s2 = a

    while not (s2 - s1) < epsi:
        x = (s1+s2)/2
        if ( x**k) > a:  s2 = x
        else:            s1 = x
    return x




def rootkf(a, k, epsi = 0.00001):
    s1 = 1
    s2 = a

    def close_enough():
        return (s2 - s1) < epsi
    
    def avr():
        return (s1 + s2)/2
    
    print(close_enough)
    print(avr)
    print(locals()) 

    while not close_enough():
        x = avr()
        if ( x**k) > a:
            s2 = x
        else:
            s1 = x
    return x





