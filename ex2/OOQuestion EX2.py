# -------------------------------------------------------------------------------
# Name:        OO class ex2
# Purpose:
#
# Author:      
#
# Created:     
#-------------------------------------------------------------------------------

class Question:
    ''' base class for different kinds of Questions
    '''
    ID_ratz = 0 #  class\static variable

    def __init__ (self, q_txt, answer_type , **kw):
        print('CTOR Question' + repr(kw))
        super().__init__(**kw)
        Question.ID_ratz += 1
        self.ID = Question.ID_ratz
        self.q_txt = q_txt
        self.user_answer = None
        self.answer_type = answer_type

    def __repr__ (self):
        sFormat = 'Question( {0!r} )'
        s = sFormat.format(self.q_txt)
        return s
    
    def __str__ (self):
        sFormat = 'Q({0:^3d}) {1:s}'
        s = sFormat.format(self.ID, self.q_txt)
        return s
    
    def show_question (self):
        ''' show_question (self):
        can overwrite         with Mixin !!!
        '''
        print( self )

    # can overwrite with Mixin
    def get_input(self):
        ''' get_input(self)
            can overwrite with Mixin !!!
            :return: user answer (can be empty string ==> user did not enter anything
            '''
        return input("Enter your answer : ")

    def get_answer (self):
        ''' get_answer (self)
            can overwrite with Mixin !!!
        '''
        a = self.get_input() # get input from user
        a = convert_answer_type(a, self.answer_type)
        self.user_answer = a
        return self.user_answer

    def response (self):
        ''' response (self)
            can overwrite with Mixin !!!
            show response to user
        '''
        if self.user_answer:
            print('your answer is: ' + str(self.user_answer))
        else:
            print('No answer')


    def ask_question(self):
        ''' ask_question(self):
        Main Template function
        '''
        print('\n')
        self.show_question()
        self.get_answer()
        self.response()
        return self.user_answer

def  convert_answer_type( answer, ans_type ):
    '''
    ans_type = int | float | str | [type] | ant_type | any_function
    answer is a string => convert  it ito type
    :return: new answer or None
    '''
    if ans_type is str: return answer
    try:
        if isinstance(ans_type,  list):
            ls1 = answer.split()
            ls2 = list(map(ans_type[0], ls1))
            return ls2
        return ans_type(answer)

    except ValueError as e:
        print('Error !!! ' + str(e))
        return None



class Q_Possible_Answers(Question):
    '''  class Q_Possible_Answers ( Question )
    has some possibe answers
    '''

    def __init__(self, list_answers, **kw):
        print('CTOR Q_Possible_Answers' + repr(kw))
        super().__init__(**kw)
        self.list_answers = list_answers

    def __repr__(self):
        sFormat = 'Q_Possible_Answers( {0!r} , {1!r} )'
        s = sFormat.format(self.q_txt, self.list_answers)
        return s

    def show_question(self):
        # YOU - use the following method: show_possible_answers

    def show_possible_answers(self):
        # YOU

    def response(self):
        if not self.user_answer:
            print('No Answer')
            return
        k = self.user_answer
        try:
            k = self.user_answer
            if k:
                ans = self.list_answers[k - 1]
            else:
                ans = ' ILLEGAL'
        except IndexError as e:
            print('IndexError: ' + str(e))
            print('!!! Problem = answer must be in range')
            return

        print("Your Answer is: " + ans)

class Q_Possible_Answers_Multi( ??? ):
    '''  class Q_Possible_Answers_Multi ( Question )
    has some possibe answers
    '''
    # YOU
    
 
#
#-------------   Check Mixins   --------------------
#
#

class Check_Mixin ():
    ''' Check_Mixin is an abstract data type '''
    def __init__(self, right_answer, grade_value, **kw):
        print('CTOR Check_Mixin'  + repr(kw))
        super().__init__(**kw)
        self.right_answer = right_answer
        self._grade_value = grade_value
        self._grade_user = None

    def get_grade_user (self) :
        return self._grade_user

    grade_user = property(get_grade_user)

    # YOU - define grade_value as property
    def grade_value(self):
        return self._grade_value


    def check(self):
        return self.user_answer != None

    def compute_grade(self):
        self._grade_user = 0
        return 0

class Check_EQ_Mixin (Check_Mixin):
    def __init__(self, **kw):
        print('CTOR Check_Mixin'  + repr(kw))
        super().__init__(**kw)

    def check ( self ):
        # YOU check if the value is equal to right_answer
        pass

    def compute_grade(self):
        # YOU compute grade – if user_answer is equalt to right_answer then 
        # the  grade is grade_value otherwise 0 
        # set calculated grade to grade_user
        pass
        
class Check_Range_Mixin( Check_Mixin ):
    def __init__(self, range_from, range_to, **kw):
        print('CTOR Check_Range_Mixin')
        # YOU complete CTOR = constructor
        pass

    def check(self):
        # YOU – return True if user_answer is in range
        pass
        
    def compute_grade(self):
        # YOU – if user answer is exactly like right_answer then the grade is grade_value
        # if within the range but not exactly right_answer then subtract 2 points – otherwise 0
        pass
class Check_American_Mixin (Check_EQ_Mixin): # Q_Possible_Answers
    def __init__ ( self, **kw ):
        # YOU
        pass

    def compute_grade(self):
        # YOU if user_answer is equal to the right_answer then return grade_value
        # otherwise subtract (1 divided by the length)  * grade_value
        # do not forget tp set grade_user to the computed grade
        pass

class Check_American_Multi_Mixin(???):  #
    pass


#
#
#-------------   Test Questions   --------------------
#
#
class Test_Q_Eq (???, Question ):
    '''q_txt=<str> , right_answer= <int>, grade_value=<int>'''
    def __init__ (self, **kw):
        # YOU
        

class Test_Q_Range (???, ???):
    # YOU

class Test_Q_American (...):
    # YOU

class Test_Q_American_Multi (...):
   # YOU

 
#
#
# -------------   Sheelon Test Classes   --------------------
#
#

class Sheelon:

    def __init__(self, *list_Qs):
        self.list_Qs = list_Qs

    def run_sheelon(self):
        for  q in self.list_Qs:
            q.ask_question()

    def show (self):
        for q in self.list_Qs:
            print(str(q) + ' => ' + str(q.user_answer))

    def do(self):
        self.run_sheelon()
        self.show()

    def __call__(self):
        ''' makes Sheelon and Test a function like'''
        # YOU

   def __len__(self):
        ''' return number of Qs in Sheelon '''
        # YOU


class Sheelon_Test(Sheelon):
            
    def show (self):
        final_grade = 0

        for q in self.list_Qs:
            q.compute_grade()

            if q.user_answer is not None:
                final_grade += q.grade_user
                sFormat = '{0} : {1}  grade= {2:2.0f} out of= {3}'
                s = sFormat.format(q.ID, q.user_answer, q.grade_user, q.grade_value)
                print(s)
            else:
                print(str(q) + ' ==> No user answer')

        print('Final Grade: ' + str(final_grade))


    

 

def test():
    global q1, q2, q3, q4, q5, q6, q7, q8, q9, test1, sheelon1

    print('q1')
    q1 = Question(q_txt='What is your name ?', answer_type=str)

    print('q2')
    q2 = Question(q_txt='What is your address ?', answer_type=str)

    print('q3')
    ls1_answers = ['Haifa', 'Aco', 'TelAviv', 'Jerusalem']
    q3 = Q_Possible_Answers(q_txt='Which is your loved city?', list_answers=ls1_answers, answer_type=int)


    print('q4')
    q4 = Test_Q_Eq(q_txt="How many days in the six days war?",
                     right_answer= 6, grade_value=10, answer_type=int)

    print('q5')
    q5 = Test_Q_American(q_txt='Which is the capital of Israel?', list_answers=ls1_answers,
                         right_answer= 4, grade_value=12, answer_type=int)

    print('q6')
    q6 = Test_Q_Range(q_txt='What is the value of PI?', range_from=3.14, range_to=3.15,
                      right_answer=3.1417, grade_value=8, answer_type=float)


    print('q7')
    ls2_answers = ['Haifa', 'Aco', 'TelAviv', 'Jerusalem', 'Eilat', 'Arad', 'Gedera']
    q7 = Test_Q_American_Multi(q_txt='Which are big cities?', list_answers=ls2_answers, grade_value=12,
                               right_answer=[1,3,4], answer_type=[int])


    sheelon1 = Sheelon(q1, q2, q3)
    #sheelon1.do()

    test1 = Sheelon_Test (q4, q5, q6, q7)
    #test1.do()


if __name__ == '__main__':
    test() # write tests in main
    pass
