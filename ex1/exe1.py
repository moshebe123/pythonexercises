import math
import string

def solve_ribuit(a, b, c):
    """
    solve_ribuit(a , b , c):
    params a b c: gets the factors of the equation.
    returns: tuple of the solutions, and if the isn't a solution, returns None.
    test 1: solve_ribuit(1 , 2 , 1) -> (-1.0, -1.0)
    test 2: solve_ribuit(1 , 5 , 6) -> (-3.0, -2.0)
    test 3: solve_ribuit(1 , 2 , 2) -> None
    """
    if a == 0:
        if b == 0 and c != 0:
            return None
        else:
            return (-c / b)
    else:
        det = (b ** 2) - (4 * a * c)
        if det < 0:
            return None
        elif det == 0:
            return (-b / (2 * a))
        else:
            sqrt_det = math.sqrt(det)
            print(det)
            print(sqrt_det)
            return ((-b + sqrt_det) / (2 * a), (-b - sqrt_det) / (2 * a))


def str_start_end_same(s, n):
    """
    str_start_end_same(s, n):
    params s n: gets a string and a number
    returns: True is n chars in the begining are also shown at the end, False if not
    test 1: str_start_end_same('abcab', 2) -> True
    test 2: str_start_end_same('abcab', 1) -> False
    """
    return s[:n] == s[-n:]


def get_ribua_num(a, b):
    """
    get_ribua_num(a, b):
    params a b: gets indexes of the wanted result
    returns: sum of previous col and row of the given indexes
    test 1: get_ribua_num(4,5) -> 35
    test 2: get_ribua_num(3,2) -> 3
    test 3: get_ribua_num(3,1) -> 1
    """
    if a == 1 or b == 1:
        return 1
    return get_ribua_num(a - 1, b) + get_ribua_num(a, b - 1)


def how_is_string(s):
    """
    how_is_string(s):
    params s: gets a string
    returns: 1 if the string contains digits and alpha chasrs,
    2 if the string contains only digits
    3 if the string contains only alpha chars
    0 in other cases
    test 1: how_is_string('this is a nice string ( ‘hhhhh’ ), bla') -> 0
    test 2: how_is_string('gfgdfgdfgdf') -> 3
    test 3: how_is_string('12345') -> 2
    test 4: how_is_string('123fsdf45sdf') -> 1
    """
    if(s.isalnum()):
        if(s.isalpha()):
            return 3
        elif(s.isdigit()):
            return 2
        else:
            return 1
    else:
        return 0


def str_only_letters(s):
    """
    str_only_letters(s):
    params s: gets a string
    returns: the string we got but without special chars, only alpha numeric chars
    test 1: str_only_letters('this is a nice string ( \'hhhhh\' ), but we want it with only letters') -> 'thisisanicestringhhhhhbutwewantitwithonlyletters'
    test 2: str_only_letters('thisisanicestringhhhhhbutwewantitwithonlyletters') -> 'thisisanicestringhhhhhbutwewantitwithonlyletters'
    """
    return ''.join([c for c in s if c.isalpha()])


def clear_special_chars(data):
    """
    clear_special_chars(data):
    params data: gets a string
    returns: list of words while ommiting special characters
    """
    return ''.join(c for c in data if c not in string.punctuation)

BLACKLIST_WORDS = 'and or the to of not in for while if as equal a c it is are an also with'.split()
def should_be_count(word):
    """
    should_be_count(word):
    params word: gets a string word
    returns: Ture - the given word isn't in the BLACKLIST_WORDS or False - the given word is in the BLACKLIST_WORDS string
    """
    return word.lower() not in BLACKLIST_WORDS

def get_top_used_words_from_file(sFile, n):
    """
    get_top_used_words_from_file(sFile, n):
    params sFiles n: gets file name and and an integer
    returns: pair of (word, count) of the top n words from the given file 
    test 1: get_top_used_words_from_file('test.txt', 3) -> [('Python', 12), ('language', 5), ('interpreter', 3)]
    test 2: get_top_used_words_from_file('test.txt', 1) -> [('Python', 12)]
    test 3: get_top_used_words_from_file('test.txt', 5) -> [('Python', 12), ('language', 5), ('interpreter', 3), ('programming', 2), ('be', 2)]
    """
    dict = {}
    with open(sFile) as f:
        words = f.read().split()
        for word in words:
            word = clear_special_chars(word)
            if should_be_count(word):
                dict[word] = dict[word] + 1 if  word in dict else 1

    return sorted(dict.items(), key=lambda x: x[1], reverse=True)[:n]
	

def divide(a,b):
    """
    divide(a,b):
    params a b: gets two numbers
    returns: the result of dividing a with b, if b is a zero, raises an Error
    """
    if b != 0:
        return a/b
    else:
        raise ZeroDivisionError("Unable to divide by zero")

OPERANDS_FUNCS = {
	'+': (lambda a,b: a+b),
	'-': (lambda a,b: a-b),
	'*': (lambda a,b: a*b),
	'/': (lambda a,b: divide(a,b))
}

def eval_post_fix(expression):
    """
    eval_post_fix(expression):
    params expression: gets a string of anmath expression
    returns: result of the calulated expression
    test 1: eval_post_fix('34+95-*') -> 28
    test 2: eval_post_fix('30/95-*') -> Error: Unable to divide by zero
    test 3: eval_post_fix('31/95-') -> Error: Missing operand
    """
    calc_stack = []

    try:
        for c in expression:
            if c.isdigit():
                calc_stack.append(int(c))
            elif c in OPERANDS_FUNCS:
                b,a = calc_stack.pop(), calc_stack.pop()
                calc_stack.append(OPERANDS_FUNCS[c](a,b))
            else:
                raise ValueError("Invalid postfix expression")

        if len(calc_stack) != 1:
            raise ValueError("Missing operand")

        return calc_stack.pop()

    except BaseException as e:
        print ('Error: ' + str(e))




# copy the following lines to your file:

# answers to question 7 
# ----------------------------Q7--------------------------------

s_females = { 1, 2 , 3 , 4 , 11 ,12 }
s_males = { 5, 6 , 7 , 8 }
s_all = s_females.union(s_males)
s_natives = { 3, 11, 7, 5}
s_telaviv = { 1 , 2 , 5 , 6 }
s_haifa = { 3, 7, 8, 11 }
s_jerusalem = s_all - s_telaviv - s_haifa # not in telaviv or haifa
s_not_tel_aviv = s_all - s_telaviv
s_males_telaviv = s_males.intersection(s_telaviv)
s_females_telaviv_or_Haifa = s_females.intersection(s_telaviv.union(s_haifa))
s_are_males_in_Jerusalem = len(s_males.intersection(s_jerusalem)) > 0 # Boolean
s_are_females_not_natives_in_Jerusalem = len ((s_females - s_natives).intersection(s_jerusalem)) > 0 # Boolean
# ----------------------------Q7--------------------------------
