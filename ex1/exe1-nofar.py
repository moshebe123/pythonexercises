def solve_ribuit(a , b , c):
    """
    solve_ribuit(a , b , c):
    params a b c: gets the factors of the equation.
    returns: tuple of the solutions, and if the isn't a solution, returns None.
    test 1: solve_ribuit(1 , 2 , 1) -> (-1.0, -1.0)
    test 2: solve_ribuit(1 , 5 , 6) -> (-3.0, -2.0)
    test 3: solve_ribuit(1 , 2 , 2) -> None
    """
    ribua_res = b**2 - 4*a*c
    if(ribua_res < 0):
        return None
    else:
        res1 = (-b - (ribua_res**0.5)) / 2*a
        res2 = (-b + (ribua_res**0.5)) / 2*a
        return (res1, res2)

def str_start_end_same(s, n):
    """
    str_start_end_same(s, n):
    params s n: gets a string and a number
    returns: True is n chars in the begining are also shown at the end, False if not
    test 1: str_start_end_same('abcab', 2) -> True
    test 2: str_start_end_same('abcab', 1) -> False
    """
    if s[:n] == s[-n:]:
        return True
    else:
        return False

def get_ribua_num(a, b):
    """
    get_ribua_num(a, b):
    params a b: gets indexes of the wanted result
    returns: sum of previous col and row of the given indexes
    test 1: get_ribua_num(4,5) -> 35
    test 2: get_ribua_num(3,2) -> 3
    test 3: get_ribua_num(3,1) -> 1
    """
    if a == 1 or b == 1:
        return 1
    else:
        return get_ribua_num(a-1, b) + get_ribua_num(a, b-1)
        
def how_is_string(s):
    """
    how_is_string(s):
    params s: gets a string
    returns: 1 if the string contains digits and alpha chasrs,
    2 if the string contains only digits
    3 if the string contains only alpha chars
    0 in other cases
    test 1: how_is_string('this is a nice string ( ‘hhhhh’ ), bla') -> 0
    test 2: how_is_string('gfgdfgdfgdf') -> 3
    test 3: how_is_string('12345') -> 2
    test 4: how_is_string('123fsdf45sdf') -> 1
    """
    if(s.isalnum()):
        if(s.isalpha()):
            return 3
        elif(s.isdigit()):
            return 2
        else:
            return 1 
    else:
        return 0

def str_only_letters(s):
    """
    str_only_letters(s):
    params s: gets a string
    returns: the string we got but without special chars, only alpha numeric chars
    test 1: str_only_letters('this is a nice string ( \'hhhhh\' ), but we want it with only letters') -> 'thisisanicestringhhhhhbutwewantitwithonlyletters'
    test 2: str_only_letters('thisisanicestringhhhhhbutwewantitwithonlyletters') -> 'thisisanicestringhhhhhbutwewantitwithonlyletters'
    """
    not_chars = ['(',')','\'','\"',' ', ',']
    for i in not_chars:
        if i in s:
            s = s.replace(i, '')
    return s
