import math

def solve_ribuit(a, b, c):
	"""
	Solve quadradic equation given its coefficients
	Params: 
		the a,b,c coefficient of the quadradic equation
	Returns:
		tuple with the equation solution or None if no such.
	"""
	# print "a:", a, "b:", b, "c:", c
	if a == 0:
		# handle cases of linear equation
		if b == 0 and c != 0:
			return None
		else:
			return (-c/b)
	else:
		det = b**2 - 4*a*c
		if det < 0:
			return None
		# commented out because of the demand to return tuple of two
		# elif det == 0: 
		# 	return (-b / 2 *a)
		else:
			sqrt_det = math.sqrt(det)
			return ((-b + sqrt_det) / 2*a, (-b - sqrt_det) / 2*a)

# Tests Q1
# print (solve_ribuit(1, -2, 1))
# print (solve_ribuit(1, -4, 3))
# print (solve_ribuit(0, 5, -3))

def str_start_end_same(s, n):
	return s[:n] == s[-n:]

# Tests Q2
# print (str_start_end_same("abcdab", 2))
# print (str_start_end_same("abcdab", 3))

def get_ribua_num(a, b):
	if a == 1 or b == 1:
		return 1
	return get_ribua_num(a-1, b) + get_ribua_num(a, b-1)

# Tests Q3
# used for get_ribua_num testing
square = ((1, 1, 1,  1,  1),
		  (1, 2, 3,  4,  5),
		  (1, 3, 6,  10, 15),
		  (1, 4, 10, 20, 35),
		  (1, 5, 15, 35, 70))

def get_ribua_num_test(a, b):
	return square[a-1][b-1] # zero based indexing

# print (get_ribua_num_1(3,3))
# print (get_ribua_num(3,3))
# print (get_ribua_num_1(5,5))
# print (get_ribua_num(5,5))
# print (get_ribua_num_1(4,3))
# print (get_ribua_num(4,3))


def how_is_string(s):
	if s.isalpha():
		return 3
	elif s.isdigit():
		return 2
	elif s.isalnum():
		return 1
	else:
		return 0

# Tests Q4
# print (how_is_string("abc123"))
# print (how_is_string("123"))
# print (how_is_string("abc"))
# print (how_is_string("a#!@bc"))

def str_only_letters(s):
	return ''.join([ c for c in s if c.isalpha()])

# Tests Q5
# print (str_only_letters("this is a nice string ('hhhhh'), but we want it with only letters"))


