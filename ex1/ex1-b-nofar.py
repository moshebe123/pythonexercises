
def get_top_used_words_from_file(sFile, n):
    
    my_dict = {}
    myList = create_list_from_file(sFile)
    for i in myList:
        my_dict[i] = my_dict.get(i, 0) + 1
    return top_words_from_dict(my_dict,3)
    
        
def create_list_from_file(sFile):
    dont_compete = {'and', 'or', 'the','to', 'of', 'not', 'in', 'for', 'while', 'if', 'as', 'equal', 'a', 'c', 'it', 'is', 'are', 'an', 'also', 'with'}
    myList = []
    for line in sFile.readlines():
        s = ''.join([c for c in line if c.isalpha() or c == ' '])
        temp = s.split()
        for i in temp:
            if i.lower() not in dont_compete:
                myList.append(i.lower())
        
    return myList


def top_words_from_dict(my_dict,n):
    top_words = []
    values = list(my_dict.values())
    values.sort()
    list_size = len(values)
    index = list_size - n
    print(list_size)
    while not index == list_size:
        for i in my_dict:
            if my_dict.get(i,0) == values[index]:
                top_words.append((i,my_dict[i]))
        index +=1
    return top_words



def eval_post_fix(s):
    operators = ['+','-','/','*']
    myList = s.split()
    myList = queue_calc(myList,'NA')
    return myList

def queue_calc(ls,temp_oper):
    operators = ['+','-','/','*']
    if not ls:
        return 0
    else:
        val = ls.pop()
        if(val in operators):
            queue_calc(ls, val)
        elif(val.isdigit()):
            val2 = ls.pop()
            if val2.isdigit():
                ls.append(calc(val,val2,temp_oper))
            else:
                queue_calc(ls,val2)
            return ls
        else:
            print("There is an invalid input at " + i + " with the value: " + val)
            return None

def calc(x,y, operator):
    x = int(x)
    y = int(y)
    if operator == '+':
        return x+y
    elif operator == '-':
        return x-y
    elif operator == '*':
        return x*y
    else: # operator is '/':
        return x/y
