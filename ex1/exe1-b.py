import string

def clear_special_chars(data):
    """
    clear_special_chars(data):
    params data: gets a string
    returns: list of words while ommiting special characters
    """
    return ''.join(c for c in data if c not in string.punctuation)

BLACKLIST_WORDS = 'and or the to of not in for while if as equal a c it is are an also with'.split()
def should_be_count(word):
    """
    should_be_count(word):
    params word: gets a string word
    returns: Ture - the given word isn't in the BLACKLIST_WORDS or False - the given word is in the BLACKLIST_WORDS string
    """
    return word.lower() not in BLACKLIST_WORDS

def get_top_used_words_from_file(sFile, n):
    """
    get_top_used_words_from_file(sFile, n):
    params sFiles n: gets file name and and an integer
    returns: pair of (word, count) of the top n words from the given file 
    test 1: get_top_used_words_from_file('test.txt', 3) -> [('Python', 12), ('language', 5), ('interpreter', 3)]
    test 2: get_top_used_words_from_file('test.txt', 1) -> [('Python', 12)]
    test 3: get_top_used_words_from_file('test.txt', 5) -> [('Python', 12), ('language', 5), ('interpreter', 3), ('programming', 2), ('be', 2)]
    """
    dict = {}
    with open(sFile) as f:
        words = f.read().split()
        for word in words:
            word = clear_special_chars(word)
            if should_be_count(word):
                dict[word] = dict[word] + 1 if  word in dict else 1

    return sorted(dict.items(), key=lambda x: x[1], reverse=True)[:n]
	

def divide(a,b):
    """
    divide(a,b):
    params a b: gets two numbers
    returns: the result of dividing a with b, if b is a zero, raises an Error
    """
    if b != 0:
        return a/b
    else:
        raise ZeroDivisionError("Unable to divide by zero")

OPERANDS_FUNCS = {
	'+': (lambda a,b: a+b),
	'-': (lambda a,b: a-b),
	'*': (lambda a,b: a*b),
	'/': (lambda a,b: divide(a,b))
}

def eval_post_fix(expression):
    """
    eval_post_fix(expression):
    params expression: gets a string of anmath expression
    returns: result of the calulated expression
    test 1: eval_post_fix('34+95-*') -> 28
    test 2: eval_post_fix('30/95-*') -> Error: Unable to divide by zero
    test 3: eval_post_fix('31/95-') -> Error: Missing operand
    """
    calc_stack = []

    try:
        for c in expression:
            if c.isdigit():
                calc_stack.append(int(c))
            elif c in OPERANDS_FUNCS:
                b,a = calc_stack.pop(), calc_stack.pop()
                calc_stack.append(OPERANDS_FUNCS[c](a,b))
            else:
                raise ValueError("Invalid postfix expression")

        if len(calc_stack) != 1:
            raise ValueError("Missing operand")

        return calc_stack.pop()

    except BaseException as e:
        print ('Error: ' + str(e))




# copy the following lines to your file:

# answers to question 7 
# ----------------------------Q7--------------------------------

s_females = { 1, 2 , 3 , 4 , 11 ,12 }
s_males = { 5, 6 , 7 , 8 }
s_all = s_females.union(s_males)
s_natives = { 3, 11, 7, 5}
s_telaviv = { 1 , 2 , 5 , 6 }
s_haifa = { 3, 7, 8, 11 }
s_jerusalem = s_all - s_telaviv - s_haifa # not in telaviv or haifa
s_not_tel_aviv = s_all - s_telaviv
s_males_telaviv = s_males.intersection(s_telaviv)
s_females_telaviv_or_Haifa = s_females.intersection(s_telaviv.union(s_haifa))
s_are_males_in_Jerusalem = len(s_males.intersection(s_jerusalem)) > 0 # Boolean
s_are_females_not_natives_in_Jerusalem = len ((s_females - s_natives).intersection(s_jerusalem)) > 0 # Boolean
# ----------------------------Q7--------------------------------
