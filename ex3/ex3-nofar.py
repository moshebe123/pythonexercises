

def map_pl ( ls_in, fn_check, fn_compute):
    """

    """
    new_ls = []
    for i in ls_in:
        if isinstance(i, list):
            new_ls.append(map_pl(i,fn_check, fn_compute))
        else:
            if fn_check(i):
                new_ls.append(fn_compute(i))
            else:
                new_ls.append(i)
    return new_ls

def list321(n):
    new_ls = []
    while not n == 1:
        if n %2 == 0:
            n = n/2
        else:
            n = (n*3) +1
        new_ls.append(n)
    return new_ls

def f321_with_cache():
    dict_nums = {}
    def cache321(n):
        if n == 1:
            return 0

        return 1
    # body of f321_with_cache
    do321.get_cache = 1 # return the dictionary
    return cache321


ls_none = []  # global variable
def check_none_wrapper(func):
    def wrf (*args, **kwargs):
        print(args, kwargs)

        global ls_none
        ls_none = []
        for i,k in enumerate(args):
            if k is None:
                ls_none.append(i)
                print("Positional arg no %d is None" %i)

        _result = func(*args, **kwargs)

        return _result

    return wrf



def do_calc( k, a, b):
    """

    """
    cases = {1: (lambda : a+b), 2: (lambda : a-b), 3: (lambda : a*b), 4: (lambda :  a/b), 'default': 'default'}
    print(sc_switch(k,cases))


def sc_switch (k, zDictCases):
    """
    simulate switch statement with cases in Dict
    'default' is used if case was not found
    if 'default is not defined in
    :param zDictCases: dictionary with cases
    :param k: selected case
    :return:
    """
    if k in zDictCases:
        fn = zDictCases[k]
        return fn()

    if 'default' in zDictCases :
        fn = zDictCases['default']
        return fn()

    return None


def test_do_calc():
    for k in range(1,5):
        if k == 1: r1 = do_calc(1,7,4)
        if k == 2: r2 = do_calc(2,7,4)
        if k == 3: r3 = do_calc(3,7,4)
        if k == 4: r4 = do_calc(4,7,4)

    return (r1, r2, r3, r4)
test_do_calc()

def add3 (a=None, b=None, c=None):
    if a==None : a = 0
    if b==None : b = 0
    if c==None : c = 0
    return a+b+c

def test_check_none():
    for k in range(1,5):
        if k == 1:
            add3_with_check = check_none_wrapper(add3)
            r1 = add3_with_check(1,2,3)
        if k == 2:
            add3_with_check = check_none_wrapper(add3)
            r2 = add3_with_check(1,2, None)
        if k == 3:
            add3_with_check = check_none_wrapper(add3)
            r3 = add3_with_check(1, b=None)
        if k == 4:
            add3_with_check = check_none_wrapper(add3)
            r4 = add3_with_check(1,None, c=None)
    return (r1, r2, r3, r4)


print(test_check_none())

print(list321(7))
ls = [ 2, -3, 4, [ -5, 6, -7 ], 11, [ 8, -9, 10 ] ]
def test1_map_pl():
    return map_pl(ls, (lambda x: x < 0), (lambda x: 0-x))
assert test1_map_pl() == [ 2, 3, 4, [5, 6, 7 ], 11, [ 8, 9, 10 ]], "bla blaa"
#returns the value:  [ 2, 3, 4, [5, 6, 7 ], 11, [ 8, 9, 10 ]]

def test2_map_pl ():
     return map_pl(ls, (lambda x: x < 0), (lambda x: 0)) #all negative numbers are replaced with 0
assert test2_map_pl() == [ 2, 0, 4, [ 0, 6, 0 ], 11, [ 8, 0, 10 ] ], "bla"
#returns the value:  [ 2, 0, 4, [ 0, 6, 0 ], 11, [ 8, 0, 10 ] ]

def test3_map_pl ():
    return map_pl(ls, (lambda x: abs(x) < 9), (lambda x: x*x))
    #all numbers whose absolute values are less than 9 are replaced with x * x
assert test3_map_pl() == [ 4, 9, 16, [25, 36, 49 ], 11, [ 64, -9, 10 ]], "bla" #error: -9 to 9

def test4_map_pl (z):
    return map_pl(ls, (lambda x: abs(x) < z), (lambda x: x+x))
    #all numbers whose absolute values are  less than z are replaced with x + x
assert test4_map_pl (7)  == [4, -6, 8, [-10, 12, -7], 11, [8, -9, 10]], "bla"
