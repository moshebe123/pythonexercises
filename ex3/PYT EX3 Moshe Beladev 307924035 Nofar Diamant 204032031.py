ls = [2, -3, 4, [-5, 6, -7], 11, [8, -9, 10]]


def map_pl(ls_in, fn_check, fn_compute):
    """
    map_pl(ls_in, fn_check, fn_compute):
    params ls_in, fn_check, fn_compute: gets a list, function to check something on the numbers in the list, and a
    function that will compute a new value and add it to the new list in case the check returns true
    returns: a new list with the changed values
    test 1: map_pl(ls, (lambda x: x < 0) (lambda x: 0-x)) -> [ 2, 3, 4, [5, 6, 7 ], 11, [ 8, 9, 10 ]]
    test 2: map_pl(ls, (lambda x: x < 0), (lambda x: 0)) -> [ 2, 0, 4, [ 0, 6, 0 ], 11, [ 8, 0, 10 ] ]
    test 3: map_pl(ls, (lambda x: abs(x) < 9), (lambda x: x*x)) -> [ 4, 9, 16, [25, 36, 49 ], 11, [ 64, -9, 10 ]
    test 4: map_pl(ls, (lambda x: abs(x) < z), (lambda x: x+x)) -> [4, -6, 8, [-10, 12, -7], 11, [8, -9, 10]]
    """
    result = []

    for i in ls_in:
        if isinstance(i, list):
            result.append(map_pl(i, fn_check, fn_compute))
        else:
            if fn_check(i):
                result.append(fn_compute(i))
            else:
                result.append(i)
    return result


def test(func, exptected_result, *args):
    assert func(*args) == exptected_result, "Invalid test"


def test1_map_pl():
    return map_pl(ls, (lambda x: x < 0), (lambda x: 0 - x))


def test2_map_pl():  # all negative numbers are replaced with 0
    return map_pl(ls, (lambda x: x < 0), (lambda x: 0))


def test3_map_pl():  # all numbers whose absolute values are  less than 9 are replaced with x * x
    return map_pl(ls, (lambda x: -9 < x < 9), (lambda x: x * x))


def test4_map_pl (z): # all numbers whose absolute values are  less than z are replaced with x + x
    return map_pl(ls, (lambda x: abs(x) < z), (lambda x: x+x))


test(test1_map_pl, [2, 3, 4, [5, 6, 7], 11, [8, 9, 10]])
test(test2_map_pl, [2, 0, 4, [0, 6, 0], 11, [8, 0, 10]])
test(test3_map_pl, [4, 9, 16, [25, 36, 49], 11, [64, -9, 10]])
test(test4_map_pl, [4, -6, 8, [-10, 12, -7], 11, [8, -9, 10]], 7)


def list321(n):
    """
    list321(n):
    params n: gets an integer number
    returns: a list of steps according to a specific set of rules
    test 1: list321(13) -> [40, 20.0, 10.0, 5.0, 16.0, 8.0, 4.0, 2.0, 1.0]
    test 2: list321(7) -> [22, 11.0, 34.0, 17.0, 52.0, 26.0, 13.0, 40.0, 20.0, 10.0, 5.0, 16.0, 8.0, 4.0, 2.0, 1.0]
    """
    steps = []

    while n != 1:
        steps.append(n)
        n = (n * 3) + 1 if n % 2 != 0 else n // 2

    steps.append(n)
    return steps


test(list321, [13, 40, 20, 10, 5, 16, 8, 4, 2, 1], 13)
test(list321, [7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1], 7)


def f321_with_cache():
    """
    f321_with_cache():
    params: no params
    returns: the function cache321
    """
    dict_nums = {}

    def cache321(n):
        """
        cache321(n):
        params n: gets an integer
        returns: number of iteration it takes to reach from n to 1
        test 1: cache321(7) -> [(2, 1), (4, 2), (5, 5), (7, 16), (8, 3), (10, 6), (11, 14), (13, 9), (16, 4), (17, 12), (20, 7), (22, 15), (26, 10), (34, 13), (40, 8), (52, 11)]
        test 2: cache321(13) -> [(2, 1), (4, 2), (5, 5), (8, 3), (10, 6), (13, 9), (16, 4), (20, 7), (40, 8)]
        """
        if n == 1:
            return 0
        elif n in dict_nums:
            return dict_nums[n]
        else:
            result = cache321((n * 3) + 1 if n % 2 != 0 else n // 2) + 1
            dict_nums[n] = result

        return result

    cache321.get_cache = (lambda: dict_nums)
    return cache321


f321 = f321_with_cache()
f321_2 = f321_with_cache()
f321(13)
f321_2(7)
d = f321.get_cache()
d_2 = f321_2.get_cache()

test((lambda: sorted(d.items())), [(2, 1), (4, 2), (5, 5), (8, 3), (10, 6), (13, 9), (16, 4), (20, 7), (40, 8)])
test((lambda: sorted(d_2.items())), [(2, 1), (4, 2), (5, 5), (7, 16), (8, 3), (10, 6),
                                     (11, 14), (13, 9), (16, 4), (17, 12), (20, 7),
                                     (22, 15), (26, 10), (34, 13), (40, 8), (52, 11)])


# Q3
ls_none = None  # global variable


def check_none_wrapper(func):
    """
    check_none_wrapper(func):
    params func: gets a function.
    returns: the wrapper function
    """
    def wrf(*args, **kwargs):
        """
        checks if the given values are None, and if so, prints a message and add the the index to a list
        """
        print(args, kwargs)

        global ls_none
        ls_none = []
        for i, val in enumerate(args):
            if val is None:
                print('Positional arg no %d is None' % i)
                ls_none.append(i)

        for key, val in kwargs.items():
            if val is None:
                print('keyword arg ==> %s is None' % key)
                ls_none.append(key)

        _result = func(*args, **kwargs)

        print('ls_none = ', ls_none, end='\n\n')
        return _result

    return wrf


def add3(a=None, b=None, c=None):
    if a == None: a = 0
    if b == None: b = 0
    if c == None: c = 0
    return a + b + c


def test_check_none():
    for k in range(1, 5):
        if k == 1:
            add3_with_check = check_none_wrapper(add3)
            r1 = add3_with_check(1, 2, 3)
        if k == 2:
            add3_with_check = check_none_wrapper(add3)
            r2 = add3_with_check(1, 2, None)
        if k == 3:
            add3_with_check = check_none_wrapper(add3)
            r3 = add3_with_check(1, b=None)
        if k == 4:
            add3_with_check = check_none_wrapper(add3)
            r4 = add3_with_check(1, None, c=None)
    return (r1, r2, r3, r4)




# Q4

def sc_switch(k, zDictCases):
    """
    simulate switch statement with cases in Dict
    'default' is used if case was not found
    if 'default is not defined in
    :param zDictCases: dictionary with cases
    :param k: selected case
    :return:
    """
    if k in zDictCases:
        fn = zDictCases[k]
        return fn()

    if 'default' in zDictCases:
        fn = zDictCases['default']
        return fn()

    return None


def do_calc(k, a, b):
    """
    do_calc(k, a, b):
    params k a b: gets a number from 1 to 4 that indicates the operator, and two numbers that represents the operands
    test 1: do_calc(1, 7, 4) -> 11
    test 2: do_calc(2, 7, 4) -> 3
    test 3: do_calc(3, 7, 4) -> 28
    test 4: do_calc(4, 7, 4) -> 1.75
    """
    cases = {1: (lambda: a + b), 2: (lambda: a - b), 3: (lambda: a * b), 4: (lambda: a / b), 'default': 'default'}
    return sc_switch(k, cases)


def test_do_calc():
    for k in range(1, 5):
        if k == 1: r1 = do_calc(1, 7, 4)
        if k == 2: r2 = do_calc(2, 7, 4)
        if k == 3: r3 = do_calc(3, 7, 4)
        if k == 4: r4 = do_calc(4, 7, 4)

    return (r1, r2, r3, r4)


test(test_do_calc, (11, 3, 28, 1.75))

print('Good Bye and have a nice day :-)')
