#-------------------------------------------------------------------------------
# Name:         ListLast
# Purpose:      lLInked List with pointer to last
# Author:       Dr Shimon Cohen
# Created:      June 2016
# Copyright:    (c) Shimon 2016
#
# TODO:  docu... (for class: define Cyclic-Queue
#-------------------------------------------------------------------------------

import copy

class ListLast ():

    def __init__(self, item= None):
        # constructor
        self.first = None
        self.last = None
        self.n = 0

        if item == None: return

        if isinstance(item, ListLast):
            self.extend(item)
            return

        if isinstance(item, list):
            self.add_list(item)
            return

        # any other type
        self.push_first(item)

    def __str__(self):
        s = '$ '
        for p in self.gen_on_link4():
            #print(p, s)
            if len(s) > 2 :
                s += ', ' + str(p.val)
            else: s += str(p.val)
        s += ' $'
        return s

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        # YOU - length of list

    def __bool__(self):
        # YOU - TRUE if list is not empty

    def __copy__(self):
        pLL = ListLast ()
        pLL.extend(self)
        return pLL

    def __getitem__(self, k):
        # YOU

    def __iadd__(self, item):
        if isinstance(item, (ListLast, list)):
            if type(item) == list:
                item = ListLast(item)
            self.extend(item)
        else:
            self.push_last(item)
        return self

    def __setitem__(self, k, v):
        # YOU

    def __contains__(self, item):
        # YOU

    def push_to_empty (self, p):
        self.first = p
        self.last = p

    def find_link(self, val):
        for p in self.gen_on_link4():
            if p.val == val: return p
        return None

    def find_k_link(self, k):
        p = self.first
        while k:
            if not p: return None
            p = p.next
            k -= 1
        return p


    def push_first(self, item):
        p = Link4_ListLast(item)
        if self.first == None:
            self.push_to_empty(p)
            return

        p.next = self.first
        self.first = p

    def push_last (self, item):
        p = Link4_ListLast(item)
        if self.first == None:
            self.push_to_empty(p)
            return

        pLast = self.last
        pLast.next = p
        self.last = p

    def add_list(self, ls):
        ''' add_list(self, ls)
        add normal list at the end of ListLast
        '''
        for x in ls:
            self.push_last(x)

    def gen_on_link4(self):
        p = self.first
        while p :
            yield p
            p = p.next


    def extend(self, ls_list_last):
        if not ls_list_last: return self

        if self.first:
            pLast = self.last
            pLast.next = ls_list_last.first
        else:
            self.first = ls_list_last.first
            self.last = ls_list_last.last
        return self

    def go_visit(self, obj_visitor):
        # YOU

    def genLL (self):
        # YOU

    def genLL_sum (self, n):
        # YOU

class ListLastT ( ListLast ):
    def __init__(self, *args , **kwargs):
        super().__init__(*args , **kwargs )

    def __iter__(self):
        # YOU

class ListLast_Iter ():
    def __init__(self, objLL):
       # YOU

    def __next__(self):
       # YOU

class Link4_ListLast ():
    def __init__(self, val, next = None):
        self.val = val
        self.next = next

    def __str__(self):
        return str(self.val)

class VisitorLL ():
    def __init__(self):
        self.dict_nums = {}

    def __call__(self, n=None):
        # YOU

class VisitorLL_up ():
    def __init__(self):


    def __call__(self, n=None):

#
# ------------------------------------------------
#

lsL1 = ListLast( [4, 5, 6, 7, 5, 4, 5, 5, 7, 6])
lsT1 = ListLastT([8, 7, 6, 8, 5, 6, 3, 6])

def test1():
    def countN(obj, n):
        k = 0
        for x in obj:
            if x == n: k += 1
        return k

    r1 = countN(lsL1, 5)  # use __getitem__
    r2 = countN(lsT1, 6)  # use __iter__
    r3 = countN(lsL1.genLL(), 4)
    r4 = countN(lsT1.genLL(), 3)

    r5 = list(lsL1.genLL_sum(5))

    objV1 = VisitorLL()  # return counts of each number
    lsL1.go_visit(objV1)
    objV1_dict = objV1()
    r6 = list(objV1_dict.items())  # list of pairs

    objV2 = VisitorLL_up()  # count => prev < current
    lsL1.go_visit(objV2)
    r7 = objV2()  # return int

    return (r1, r2, r3, r4, r5, r6, r7)

compL1 = [3, 5, 7, 8, 9, 5, 6, 7, 4, 3]
compL2 = [1, 4, 5, 8, 3, 6, 9, 3, 5, 9]

def test2():
    r1 = # YOU
    r2 = # YOU
    r3 = # YOU
    r4 = # YOU
    r5 = # YOU
    r6 = # YOU
    r6 = map(list, r6)
    rs = [r1, r2, r3, r4, r5, r6]
    ls_rs = list(list(ls) for ls in rs)
    for ls in ls_rs: print(ls)
    return ls_rs

''' -----------------   ANSWERS  -------------
[
[7, 8, 9, 6, 7]
[16, 11, 15, 12]
[0, 0, 49, 64, 81, 0, 36, 49, 0, 0]
[0, 49, 64, 81, 0, 36, 49, 0]
[(8, 8), (8, 9), (8, 9), (9, 8), (9, 9), (9, 9)]
[[4, 4], [6, 6], [6, 6]]
]
'''








