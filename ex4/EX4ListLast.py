#-------------------------------------------------------------------------------
# Name:         ListLast
# Purpose:      lLInked List with pointer to last
# Author:       Dr Shimon Cohen
# Created:      June 2016
# Copyright:    (c) Shimon 2016
#
# TODO:  docu... (for class: define Cyclic-Queue
#-------------------------------------------------------------------------------

import copy

class ListLast ():
    """
    """

    def __init__(self, item= None):
        # constructor
        self.first = None
        self.last = None
        self.n = 0

        if item == None: return

        if isinstance(item, ListLast):
            self.extend(item)
            return

        if isinstance(item, list):
            self.add_list(item)
            return

        # any other type
        self.push_first(item)

    def __str__(self):
        s = '$ '
        for p in self.gen_on_link4():
            #print(p, s)
            if len(s) > 2 :
                s += ', ' + str(p.val)
            else: s += str(p.val)
        s += ' $'
        return s

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        k = 0
        for x in self.gen_on_link4() :
            k += 1
        return k

    def __bool__(self):
        return self.first != None

    def __copy__(self):
        pLL = ListLast ()
        pLL.extend(self)
        return pLL

    def __getitem__(self, k):
        p = self.find_k_link(k)
        if p:
            return p.val
        else:
            raise IndexError

    def __iadd__(self, item):
        if isinstance(item, (ListLast, list)):
            if type(item) == list:
                item = ListLast(item)
            self.extend(item)
        else:
            self.push_last(item)
        return self



    def __setitem__(self, k, v):
        p = self.find_k_link(k)
        if p:
            p.val = v
        else:
            raise IndexError

    def __contains__(self, item):
        p = self.find_link(item)
        return p != None

    def push_to_empty (self, p):
        self.first = p
        self.last = p

    def find_link(self, val):
        for p in self.gen_on_link4():
            if p.val == val: return p
        return None

    def find_k_link(self, k):
        p = self.first
        while k:
            if not p: return None
            p = p.next
            k -= 1
        return p


    def push_first(self, item):
        p = Link4_ListLast(item)
        if self.first == None:
            self.push_to_empty(p)
            return

        p.next = self.first
        self.first = p

    def push_last (self, item):
        p = Link4_ListLast(item)
        if self.first == None:
            self.push_to_empty(p)
            return

        pLast = self.last
        pLast.next = p
        self.last = p

    def add_list(self, ls):
        ''' add_list(self, ls)
        add normal list at the end of ListLast
        '''
        for x in ls:
            self.push_last(x)

    def gen_on_link4(self):
        p = self.first
        while p :
            yield p
            p = p.next


    def extend(self, ls_list_last):
        if not ls_list_last: return self

        if self.first:
            pLast = self.last
            pLast.next = ls_list_last.first
        else:
            self.first = ls_list_last.first
            self.last = ls_list_last.last
        return self

    def go_visit(self, obj_visitor):
        for x in self.gen_on_link4():
            obj_visitor (x.val)

    def genLL (self):

        cur = self.first
        while cur:
            yield cur.val
            cur = cur.next

    def genLL_sum (self, n):
        cur = self.first
        sum = 0
        while cur:
            if cur.val > n:
                sum += cur.val
                yield sum
            cur = cur.next

class ListLastT ( ListLast ):
    def __init__(self, *args , **kwargs):
        super().__init__(*args , **kwargs )

    def __iter__(self):
        return ListLast_Iter(self)

class ListLast_Iter ():
    def __init__(self, objLL):
        print(objLL)
        self.obj = objLL
        self.current = objLL.first
        print(self.current)

    def __next__(self):
        print(self.current)
        if self.current == None: raise StopIteration
        v = self.current.val
        self.current = self.current.next
        return v

class Link4_ListLast ():
    def __init__(self, val, next = None):
        self.val = val
        self.next = next

    def __str__(self):
        return str(self.val)

class VisitorLL ():
    def __init__(self):
        self.dict_nums = {}

    def __call__(self, n=None):
        if n == None: return self.dict_nums

        if n in self.dict_nums:
            self.dict_nums[n] += 1
        else:
            self.dict_nums[n] = 1

class VisitorLL_up ():
    def __init__(self):
        self.prev = None
        self.k = 0

    def __call__(self, n=None):
        if n == None: return self.k
        if self.prev:
            if self.prev < n: self.k += 1
        self.prev = n

class   Link_Last_Error (Exception):
        def __init__(self, sError, pList=None, pLink=None):
            super().__init__(sError)
            self.pList = pList
            self.pLink = pLink






