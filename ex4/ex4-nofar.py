compL1 = [1,2,3,67,8,9,5]
compL2 = [1,46,7,3,5,98]



#Q1
print (list(x  for x in compL1 if x > 5))

#Q2
print (list( i + j for i,j in zip(compL1, compL2) if j > 5 ))

#Q3
print (list(x*x if x > 5 else 0 for x in compL1))

#Q4
print (list(x*x if x > 5 else 0 for x in compL1 if x > 3))

#Q5
print (list((k, l) for k, l in zip(compL1, compL2) if k > 7 and l > 7))

#Q6
print (list([x + y for x in compL2 if x < 4] for y in compL1 if y < 4))