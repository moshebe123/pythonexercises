def q2_3_helper(ls): return [x**2 if x > 5 else 0 for x in ls]

compL1 = [3, 5, 7, 8, 9, 5, 6, 7, 4, 3]
compL2 = [1, 4, 5, 8, 3, 6, 9, 3, 5, 9]

def test2():
    r1 = [x for x in compL1 if x > 5]
    r2 = [a+b for a,b in zip(compL1, compL2) if b > 5]
    r3 = q2_3_helper(compL1)
    r4 = q2_3_helper( [x for x in compL1 if x > 3] )
    r5 = [(a,b) for a in compL1 if a > 7 for b in compL2 if b > 7]
    r6 = [[x+y for x in compL1 if x < 4] for y in compL2 if y < 4]
    r6 = map(list, r6)
    rs = [r1, r2, r3, r4, r5, r6]
    ls_rs = list(list(ls) for ls in rs)
    for ls in ls_rs: print(ls)
    return ls_rs

test2()

''' -----------------   ANSWERS  -------------
[
[7, 8, 9, 6, 7]
[16, 11, 15, 12]
[0, 0, 49, 64, 81, 0, 36, 49, 0, 0]
[0, 49, 64, 81, 0, 36, 49, 0]
[(8, 8), (8, 9), (8, 9), (9, 8), (9, 9), (9, 9)]
[[4, 4], [6, 6], [6, 6]]
]
'''